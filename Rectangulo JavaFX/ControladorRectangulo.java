import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class ControladorRectangulo {

    @FXML
    private Label lblOrigenY;

    @FXML
    private TextField txtAncho;

    @FXML
    private Label lblUbicacion;

    @FXML
    private Label lblOrigenX;

    @FXML
    private Button cmdPosicion;

    @FXML
    private BorderPane panel1;

    @FXML
    private Button cmdCoordenadas;

    @FXML
    private ImageView imageRectangulo;

    @FXML
    private Label lblTitulo;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Button cmdAyuda;

    @FXML
    private TextField txtAlto;

    @FXML
    private TextField txtOrigenX;

    @FXML
    private TextField txtOrigenY;

    @FXML
    private Label lblX;

    @FXML
    private Label lblAlto;

    @FXML
    private Label lblY;

    @FXML
    private Label lblAncho;

    @FXML
    private GridPane panelBotones;

    @FXML
    private TextField txtX;

    @FXML
    private TextField txtY;
    
    private ModeloRectangulo rectangulo;
    
    public ControladorRectangulo(){
    rectangulo = new ModeloRectangulo();
    }
    
    @FXML
    void actualizarCoordenadas() {
    rectangulo.setOrigenX(Float.parseFloat(txtOrigenX.getText()));
    rectangulo.setOrigenY(Float.parseFloat(txtOrigenY.getText()));
    rectangulo.setAncho(Float.parseFloat(txtAncho.getText()));
    rectangulo.setAlto(Float.parseFloat(txtAlto.getText()));
    rectangulo.setX(Float.parseFloat(txtX.getText()));
    rectangulo.setY(Float.parseFloat(txtY.getText()));
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Medidas actualizadas");
    alert.showAndWait();
    }

    @FXML
    void determinarPosicion() {
    lblUbicacion.setText(rectangulo.getPosicionPunto(rectangulo.getX(), rectangulo.getY()));
    
    }

    @FXML
    void ayuda() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Determinar la posición de un punto con respecto a un Rectángulo");
    alert.showAndWait();
    }

}
