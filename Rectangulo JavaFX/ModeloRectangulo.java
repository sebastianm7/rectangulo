/**
 * Un sencillo ejercicio geomÃ©trico con un Ãºnico objeto Rectangulo.
 * @author (Milton JesÃºs Vera Contreras - miltonjesusvc@ufps.edu.co) 
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
class ModeloRectangulo {

    /**Coordenada x del punto correspondiente a la esquina superior izquierda del Rectangulo*/
    protected float origenX=Float.POSITIVE_INFINITY;
    /**Coordenada y del punto correspondiente a la esquina superior izquierda del Rectangulo*/
    protected float origenY=Float.POSITIVE_INFINITY;
    /**Ancho del Rectangulo*/
    protected float ancho=Float.POSITIVE_INFINITY;
    /**Alto del Rectangulo*/
    protected float alto=Float.POSITIVE_INFINITY;
    
    protected float x=Float.POSITIVE_INFINITY;
    
    protected float y=Float.POSITIVE_INFINITY;

    /**
     * Constructor default
     */
    public ModeloRectangulo(){
        this.origenX = 0;
        this.origenY = 0;
        this.ancho = 0;
        this.alto = 0;
        this.x = 0;
        this.y = 0;
    }

    /**
     * Constructor con parÃ¡metros
     * @x el valor inicial para origenX
     * @y el valor inicial para origenY
     * @w el valor inicial para el ancho
     * @h el valor inicial para el alto
     */
    public ModeloRectangulo(float origenX, float origenY, float w, float h, float x, float y){
        this.origenX = origenX;
        this.origenY = origenY;
        this.ancho = w;
        this.alto = h;
        this.x = x;
        this.y = y;
    }

    /**
     * Determina la ubicaciÃ³n de un punto con coordenadas x, y respecto al Rectangulo.
     * Las posibles salidas son:
     * "Punto Dentro del RectÃ¡ngulo"
     * "Punto En Borde Superior"
     * "Punto en Borde Inferior"
     * "Punto en Borde Izquierdo"
     * "Punto en Borde Derecho"
     * "Punto Fuera del RectÃ¡ngulo"
     * 
     * @param x coordenada x del punto para el cual se requiere determinar la ubicaciÃ³n
     * @param y coordenada y del punto para el cual se requiere determinar la ubicaciÃ³n
     * @return Un String indicando la ubicacion
     */
    public String getPosicionPunto(float x, float y) {
        String ubicacion = "Ubicación Desconocida";

        if (puntoEstaDentro(x,y)){ubicacion = "Punto Dentro del Rectángulo";}
        else if(puntoEstaEnBordeSuperior(x,y)){ubicacion = "Punto En Borde Superior";}
        else if(puntoEstaEnBordeInferior(x,y)){ubicacion = "Punto en Borde Inferior";}
        else if(puntoEstaEnBordeIzquierdo(x,y)){ubicacion = "Punto en Borde Izquierdo";}
        else if(puntoEstaEnBordeDerecho(x,y)){ubicacion = "Punto en Borde Derecho";}
        else{ubicacion = "Punto Fuera del Rectángulo";}
        
        return ubicacion;
    }//fin mÃ©todo getPosiciÃ³nPunto

    /**
     * Determina si un punto de coordenadas x,y se encuentran dentro de este Rectangulo
     * @param x coordenada x del punto para el cual se requiere determinar si estÃ¡ dentro del Rectanculo
     * @param y coordenada y del punto para el cual se requiere determinar si estÃ¡ dentro del Rectanculo
     * @return false  o true segÃºn corresponda
     */
    public boolean puntoEstaDentro(float x, float y) {
        boolean tmp = false;
        if((origenX < x && origenY > y) && ((origenX+ancho) > x && origenY > y) && (origenX < x && (origenY-alto) < y) && ((origenX+ancho) > x && (origenY-alto) < y)){tmp = true;}
        return tmp;
    }//fin mÃ©todo puntoEstaDentro 

    /**
     * Determina si un punto de coordenadas x,y se encuentran en el Borde Superior de este Rectangulo
     * @param x coordenada x del punto para el cual se requiere determinar si estÃ¡ en el Borde Superior del Rectanculo
     * @param y coordenada y del punto para el cual se requiere determinar si estÃ¡ en el Borde Superior del Rectanculo
     * @return false  o true segÃºn corresponda
     */
    public boolean puntoEstaEnBordeSuperior(float x, float y) {
        boolean tmp = false;
        if((origenY == y) && (x >= origenX && x <= (origenX + ancho))){tmp = true;}
        return tmp;
    }//fin puntoEstaEnBordeSuperior

    /**
     * Determina si un punto de coordenadas x,y se encuentran en el Borde Inferior de este Rectangulo
     * @param x coordenada x del punto para el cual se requiere determinar si estÃ¡ en el Borde Inferior del Rectanculo
     * @param y coordenada y del punto para el cual se requiere determinar si estÃ¡ en el Borde Inferior del Rectanculo
     * @return false  o true segÃºn corresponda
     */
    public boolean puntoEstaEnBordeInferior(float x, float y) {
        boolean tmp = false;
        if(((origenY-alto) == y) && (x >= origenX && x <= (origenX + ancho))){tmp = true;}
        return tmp;
    }//fin puntoEstaEnBordeInferior

    /**
     * Determina si un punto de coordenadas x,y se encuentran en el Borde Izquierdo de este Rectangulo
     * @param x coordenada x del punto para el cual se requiere determinar si estÃ¡ en el Borde Izquierdo  del Rectanculo
     * @param y coordenada y del punto para el cual se requiere determinar si estÃ¡ en el Borde Izquierdo  del Rectanculo
     * @return false  o true segÃºn corresponda
     */
    public boolean puntoEstaEnBordeIzquierdo(float x, float y) {
        boolean tmp = false;
        if((origenX == x) && (y <= origenY && y >= (origenY - alto))){tmp = true;}
        return tmp;
    }//fin puntoEstaEnBordeIzquierdo

    /**
     * Determina si un punto de coordenadas x,y se encuentran en el Borde Derecho de este Rectangulo
     * @param x coordenada x del punto para el cual se requiere determinar si estÃ¡ en el Borde Derecho del Rectanculo
     * @param y coordenada y del punto para el cual se requiere determinar si estÃ¡ en el Borde Derecho del Rectanculo
     * @return false  o true segÃºn corresponda
     */
    public boolean puntoEstaEnBordeDerecho(float x, float y) {
        boolean tmp = false;
        if(((origenX+ancho) == x) && (y <= origenY && y >= (origenY - alto))){tmp = true;}
        return tmp;
    }//fin puntoEstaEnBordeDerecho

    /**
     * Determina si un punto de coordenadas x,y se encuentran fuera de este Rectangulo
     * @param x coordenada x del punto para el cual se requiere determinar si estÃ¡fuera del Rectanculo
     * @param y coordenada y del punto para el cual se requiere determinar si fuera del Rectanculo
     * @return false  o true segÃºn corresponda
     */
    public boolean puntoEstaFuera(float x, float y) {
        boolean tmp = false;
        if(((origenX > x || origenX < x) && origenY < y) || ((origenX > x || origenX < x) || (origenY-alto) < y) || (origenX > x || (origenY< y || (origenY-alto)>y)) || ((origenX+ancho) < x || (origenY< y || (origenY-alto)>y))){tmp = true;}
        return tmp;
    }//fin puntoEstaFuera

    /**Metodo de acceso a la propiedad origenX*/
    public float getOrigenX(){
        return origenX;
    }//end method getOrigenX

    /**Metodo de modificaciÃ³n a la propiedad origenX*/
    public void setOrigenX(float newOrigenX){
        this.origenX = newOrigenX;
    }//end method setOrigenX

    /**Metodo de acceso a la propiedad origenY*/
    public float getOrigenY(){
        return origenY;
    }//end method getOrigenY

    /**Metodo de modificaciÃ³n a la propiedad origenY*/
    public void setOrigenY(float origenY){
        this.origenY = origenY;
    }//end method setOrigenY

    /**Metodo de acceso a la propiedad ancho*/
    public float getAncho(){
        return ancho;
    }//end method getAncho

    /**Metodo de modificaciÃ³n a la propiedad ancho*/
    public void setAncho(float ancho){
        this.ancho = ancho;
    }//end method setAncho

    /**Metodo de acceso a la propiedad alto*/
    public float getAlto(){
        return alto;
    }//end method getAlto

    /**Metodo de modificaciÃ³n a la propiedad alto*/
    public void setAlto(float alto){
        this.alto = alto;
    }//end method setAlto
    
    public void setX(float x) {
        this.x = x;
    }
    
    public void setY(float y) {
        this.y = y;
    }
    
    public float getX() {
        return this.x;
    }
    
    public float getY() {
        return this.y;
    }
    
}//fin clase Rectangulo